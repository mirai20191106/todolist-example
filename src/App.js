import React from 'react';
import './App.css';
import TodoBox from './components/Todobox';
function App() {
  return (
    <TodoBox />
  );
}

export default App;
