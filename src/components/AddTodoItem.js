import React from 'react';
import { Form, Input, Button } from 'antd';
export default function AddTodoItem(props) {
  const [label, setLabel] = React.useState('');
  const [task, setTask] = React.useState('');
  let saveNewItem = (e) => {
    e.preventDefault()
    if (!task) {
      setLabel('Todo內容不得為空！');
    } else {
      props.saveNewItem(task)
      setTask('');
      setLabel('');
    }
  }
  console.log('AddTodoItem rendered.');
  /**
   * 這邊邏輯同TodoItem
   */
  return (
    <div className="addtodoitem">
      <Form.Item>
        <label >{label}</label><br/>
        <Input id="newItem" type="text" value={task} placeholder="吃飯睡覺打豆豆~" onChange={(e) => { setTask(e.target.value) }}></Input>
        <Button type="primary" className="pull-right" onClick={saveNewItem}>保存</Button>
      </Form.Item>
    </div>
  )

}