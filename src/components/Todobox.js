import React from 'react';
import TodoList from './TodoList';
import AddTodoItem from './AddTodoItem';
import origindata from './data.json'
export default function TodoBox(props) {

  /**
   * 利用Hook 掌握物件狀態、物件狀態變更方法(function)
   * 以data為例，data固定為物件、setData則是物件變更用的方法，
   * React.useState內傳入的參數則是data的預設值(這邊用data.json的資料當作預設值傳入)
   */
  const [data, setData] = React.useState(origindata);

  let generateGUID = () => {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      var r = Math.random() * 16 | 0,
        // eslint-disable-next-line 
        v = c === 'x' ? r : (r & 0x3 | 0x8)
      return v.toString(16)
    })
  }
  let handleToggleComplete = (taskId) => {
    for (let item of data) {
      if (item.id === taskId) {
        item.complete = item.complete === "true" ? "false" : "true"
      }
    }
    setData([...data]);
  }
  let handleTaskDelete = (taskId) => {
    let ndata = data.filter(task => task.id !== taskId)
    setData([...ndata]);
  }
  let handleAddTodoItem = (task) => {
    let newItem = {
      id: generateGUID(),
      task,
      complete: "false"
    }

    let ndata = data.concat([newItem])
    setData([...ndata]);
  }

  /**
   * 標準的React變更方法，需要透過一層層將父元件的物件狀態變更方法傳到下層元件內
   * 當下層元件內物件變更時，再透過由上層傳入的方法進行物件狀態的變更。
   * 所以這邊可以看到將 handleToggleComplete、handleTaskDelete 傳給TodoList、
   * handleAddTodoItem 傳給 AddTodoItem
   */
  console.log('Todobox rendered', data);
  return (
    <div className="App">
      <div className="well">
        <h1 className="text-center">React TodoList</h1>
        <TodoList data={data} toggleComplete={handleToggleComplete} deleteTask={handleTaskDelete} />
        <AddTodoItem saveNewItem={handleAddTodoItem} />
      </div>
    </div>
  )

}