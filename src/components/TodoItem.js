import React from 'react';
import { Row, Col, Checkbox, Button } from 'antd';

export default function TodoItem(props) {

  let task = props.task
  let itemChecked
  if (props.complete === "true") {
    task = <del>{task}</del>
    itemChecked = true
  } else {
    itemChecked = false
  }
  /**
   * 最終於TodoItem進行資料變更時，再呼叫上層(TodoList)傳來的方法進行資料變更
   * (但實際上呼叫到的方法是從TodoBox那來的)，所以執行後會跳到TodoBox作處理，
   * 資料變更後再觸發TodoBox render
   */
  console.log('TodoItem rendered.',props);
  return (
    <li className="list-group-item">
      <Row>
        <Col span={12}>
          <Checkbox checked={itemChecked} onChange={() => {
            props.toggleComplete(props.taskId);
          }
          } /> {task}
        </Col>
        <Col span={12}>
          <Button type="danger" className="pull-right" onClick={() => {
            props.deleteTask(props.taskId)
          }}>删除</Button>
        </Col>
      </Row>
    </li>
  )
}