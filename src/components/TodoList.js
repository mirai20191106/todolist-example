import React from 'react';
import TodoItem from './TodoItem';
export default function TodoList(props) {

  /**
   * 在TodoList內，為了要讓各個TodoItem可以呼叫物件狀態變更方法，
   * 所以需要再將Todobox傳給TodoList的方法再傳給TodoItem
   */
  let taskList = props.data.map(listItem =>
    <TodoItem taskId={listItem.id}
      key={listItem.id}
      task={listItem.task}
      complete={listItem.complete}
      toggleComplete={props.toggleComplete}
      deleteTask={props.deleteTask} />
  )
  console.log('TodoList rendered.', props.data);
  return (
    <ul className="list-group">
      {taskList}
    </ul>
  )

}